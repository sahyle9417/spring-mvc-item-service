package hello.itemservice.web.form;

import hello.itemservice.domain.item.DeliveryCode;
import hello.itemservice.domain.item.Item;
import hello.itemservice.domain.item.ItemRepository;
import hello.itemservice.domain.item.ItemType;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/form/items")
@RequiredArgsConstructor
public class FormItemController {

    private final ItemRepository itemRepository;

    // 메서드가 반환한 객체를 동일 클래스 내 모든 메서드의 model attribute 에 추가
    // 동적으로 변하지 않는다면 성능을 고려해서 static 하게 해놓는게 더 좋다
    @ModelAttribute("regions")
    public Map<String, String> regions() {
        Map<String, String> regions = new LinkedHashMap<>();
        regions.put("SEOUL", "서울");
        regions.put("BUSAN", "부산");
        regions.put("JEJU", "제주");
        return regions;
    }

    // 메서드가 반환한 객체를 동일 클래스 내 모든 메서드의 model attribute 에 추가
    // 동적으로 변하지 않는다면 성능을 고려해서 static 하게 해놓는게 더 좋다
    @ModelAttribute("itemTypes")
    public ItemType[] itemTypes() {
        // ENUM 모든 값 리스트로 반환
        return ItemType.values();
    }

    // 메서드가 반환한 객체를 동일 클래스 내 모든 메서드의 model attribute 에 추가
    // 동적으로 변하지 않는다면 성능을 고려해서 static 하게 해놓는게 더 좋다
    @ModelAttribute("deliveryCodes")
    public List<DeliveryCode> deliveryCodes() {
        List<DeliveryCode> deliveryCodes = new ArrayList<>();
        deliveryCodes.add(new DeliveryCode("FAST", "빠른 배송"));
        deliveryCodes.add(new DeliveryCode("NORMAL", "일반 배송"));
        deliveryCodes.add(new DeliveryCode("SLOW", "느린 배송"));
        return deliveryCodes;
    }

    @GetMapping
    public String items(Model model) {
        List<Item> items = itemRepository.findAll();
        model.addAttribute("items", items);
        return "/form/items";
    }

    @GetMapping("/{itemId}")
    public String item(@PathVariable long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "/form/item";
    }

    @GetMapping("/add")
    public String addForm(Model model) {
        model.addAttribute("item", new Item());
        return "/form/addForm";
    }

    // @PostMapping("/add")
    public String addItemOldV1(@RequestParam String itemName,
                            @RequestParam Integer price,
                            @RequestParam Integer quantity,
                            Model model) {

        Item item = new Item();
        item.setItemName(itemName);
        item.setPrice(price);
        item.setQuantity(quantity);

        itemRepository.save(item);

        model.addAttribute("item", item);

        return "basic/item";
    }

    // @ModelAttribute("item") 사용하면 자동으로 해당 객체 model 에 등록
    // 즉, request 에게서 Item 객체를 받아서 model 에 자동 등록
    // model.addAttribute("item", item);
    // @PostMapping("/add")
    public String addItemOldV2(@ModelAttribute("item") Item item) {
        itemRepository.save(item);
        return "basic/item";
    }

    // @ModelAttribute 에서 name 지정하지 않으면 class 명(Item) 맨 앞글자 소문자로 바꾼 것(item)으로 지정됨
    // @PostMapping("/add")
    public String addItemOldV3(@ModelAttribute Item item) {
        itemRepository.save(item);
        return "basic/item";
    }

    // @ModelAttribute 생략 가능 (지나치게 추상화되므로 권장하지 않음)
    // @PostMapping("/add")
    public String addItemOldV4(Item item) {
        itemRepository.save(item);
        return "basic/item";
    }

    // @PostMapping("/add")
    public String addItemOldV5(Item item) {
        itemRepository.save(item);
        return "redirect:/basic/items/" + item.getId();
    }

    @PostMapping("/add")
    public String addItem(@ModelAttribute Item item, RedirectAttributes redirectAttributes) {
        Item savedItem = itemRepository.save(item);
        // view 경로에 적힘
        redirectAttributes.addAttribute("itemId", savedItem.getId());
        // view 경로에 포함되지 않은 경우 query param에 추가됨 (?ststus=true)
        redirectAttributes.addAttribute("status", true);
        return "redirect:/form/items/{itemId}";
    }

    @GetMapping("/{itemId}/edit")
    public String editForm(@PathVariable Long itemId, Model model) {
        Item item = itemRepository.findById(itemId);
        model.addAttribute("item", item);
        return "/form/editForm";
    }

    @PostMapping("/{itemId}/edit")
    public String edit(@PathVariable Long itemId, @ModelAttribute Item item) {
        itemRepository.update(itemId, item);
        return "redirect:/form/items/{itemId}";
    }

}

